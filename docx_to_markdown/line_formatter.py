from re import sub
from typing import Iterable, NamedTuple


class LineFormatter(NamedTuple):
    is_remove_multispaces: bool
    is_remove_spaces_before_dots: bool
    is_escape_chars: bool
    is_escape_lt_gt: bool

    @property
    def is_false(self) -> bool:
        return all(getattr(self, x) is False for x in self._fields)

    @staticmethod
    def _remove_multispaces(line: str) -> str:
        return sub(r"\s+", " ", line)

    @staticmethod
    def _escape_chars(line: str) -> str:
        for char in ("*", "-", "_"):
            line = line.replace(f"{char}", f"\\{char}")
        return line

    @staticmethod
    def _remove_spaces_before_dots(line: str) -> str:
        return sub(r"\s\.", ".", line)

    @staticmethod
    def _escape_lt_gt(line: str):
        return sub(r"<(?!/?br>|/?sub>|/?sup>)([^<>]*)(?<!<br)>", r"\\<\1\\>", line)

    def _format_line(self, line: str) -> str:
        if self.is_remove_multispaces:
            line = self._remove_multispaces(line)
        if self.is_remove_spaces_before_dots:
            line = self._remove_spaces_before_dots(line)
        if self.is_escape_chars:
            line = self._escape_chars(line)
        if self.is_escape_lt_gt:
            line = self._escape_lt_gt(line)
        return line.replace("\n", "<br>")

    def format_lines(self, lines: Iterable[str] = None) -> list[str] | None:
        if lines is None:
            return
        return [self._format_line(line) for line in lines]
