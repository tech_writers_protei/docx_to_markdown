#!/usr/bin/python
# -*- coding: cp1251 -*-
from pathlib import Path
from shutil import rmtree
from typing import Iterator, TypeAlias
from zipfile import ZIP_DEFLATED, ZipFile

from lxml import etree
# noinspection PyProtectedMember
from lxml.etree import ElementBase, _ElementTree

from docx_to_markdown.qualified_name import _ns, fqdn
from docx_to_markdown.xml_formatter import get_all_text
from toml_parser import LineFormatter, TomlConfig

ET: TypeAlias = _ElementTree | None
EB: TypeAlias = ElementBase | None
PathLike: TypeAlias = str | Path


class CoreDocument:
    def __init__(self, path: PathLike):
        if isinstance(path, str):
            path: Path = Path(path).resolve()

        self.path: Path = path

        for k, v in _ns.items():
            etree.register_namespace(k, v)

        self._zip_file: ZipFile = ZipFile(self.path, "r", ZIP_DEFLATED)
        self.toml_config: TomlConfig = TomlConfig(Path(__file__).parent.joinpath("config/config.toml"))
        self.path_dir: Path = path.parent.joinpath(self.toml_config["folder_temp"])

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.path})>"

    def __str__(self):
        return f"{self.__class__.__name__}: {self.path}"

    def unarchive(self):
        self.path_dir.mkdir(exist_ok=True)
        self._zip_file.extractall(self.path_dir)

    def delete_temp_archive(self):
        rmtree(self.path_dir, True)

    @property
    def name(self):
        return self.path.stem


class UnzippedFile:
    def __init__(self, name: str, core_document: CoreDocument):
        self._name: str = name
        self._core_document: CoreDocument = core_document

    @property
    def toml_config(self):
        return self._core_document.toml_config

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._name}, {repr(self._core_document)})>," \
               f" {self._core_document.path}"

    def __str__(self):
        return f"{self.__class__.__name__}: {self._name}, {str(self._core_document)}, {self._core_document.path}"

    @property
    def full_path(self) -> Path:
        """The full path to the file in the unpacked archive."""
        return self._core_document.path_dir.joinpath(self._name)


class XmlFile(UnzippedFile):
    def __init__(self, name: str, core_document: CoreDocument):
        super().__init__(name, core_document)
        self.content: EB = etree.parse(self.full_path).getroot()

    def read(self):
        self.content = etree.parse(self.full_path).getroot()

    def get_children_elements(self, tag: str) -> list[ElementBase]:
        return list(self.content.iterdescendants(fqdn(tag)))


class XmlFilePart:
    def __init__(self, tag: str, parent: ElementBase, idx: int | None = None):
        if idx is None:
            idx: int = -1
        self._parent: ElementBase = parent
        self._tag: str = tag
        self._index: int = idx
        self.content: EB = None
        self.read()

    def read(self):
        if self._index == -1:
            self.content = list(self._parent.iterdescendants(fqdn(self._tag)))[0]
        else:
            self.content = list(self._parent.iterdescendants(fqdn(self._tag)))[self._index]

    def get_children_elements(self, tag: str | None = None) -> list[ElementBase]:
        return list(self.content.iterdescendants(fqdn(tag)))


class XmlDocument(XmlFile):
    def __init__(self, core_document: CoreDocument):
        self._name: str = "word/document.xml"
        super().__init__(self._name, core_document)

        _path_dir: Path = self._core_document.path_dir
        _folder_tables: str = self.toml_config["folder_tables"]

        self.table_dir: Path = _path_dir.parent.joinpath(f"{_folder_tables}_{core_document.name}")

        if not self.table_dir.exists():
            self.table_dir.mkdir(parents=True, exist_ok=True)

        for file in self.table_dir.iterdir():
            file.unlink()

    def __repr__(self):
        return f"<{self.__class__.__name__}({repr(self._core_document)})>"

    def __str__(self):
        return f"{self.__class__.__name__}: {self._name}"

    def __len__(self):
        return len(list(self.content.iterdescendants(fqdn("w:tbl"))))

    def __iter__(self) -> Iterator['XmlTable']:
        return iter(XmlTable(self, table_index) for table_index in range(len(self)))

    def parse_document(self):
        xml_table: XmlTable
        for xml_table in iter(self):
            xml_table.read()
            xml_table.write_to_file()


class XmlTable(XmlFilePart):
    def __init__(self, xml_document: XmlDocument, table_index: int):
        tag: str = "w:tbl"
        super().__init__(tag, xml_document.content, table_index)
        self._table_index: int = self._index
        self._xml_document: XmlDocument = xml_document
        self._content: EB = None

    def __str__(self):
        return "\n".join(self._to_lines())

    @property
    def toml_config(self):
        return self._xml_document.toml_config

    @property
    def _num_rows(self) -> int:
        return len(self.get_children_elements("w:tr"))

    def __iter__(self):
        return iter(XmlTableRow(self, row_index) for row_index in range(self._num_rows))

    @property
    def xml_rows(self) -> list['XmlTableRow']:
        return list(iter(self))

    def __len__(self):
        return len(self.xml_rows[0])

    def _to_lines(self) -> list[str]:
        content: list[str] = []
        for xml_table_row in self.xml_rows:
            xml_table_row.read()
            _: str = " | ".join(xml_table_row.cells_text())
            content.append(f"| {_} |")

        header: str = "|".join(["-----"] * len(self))
        line_formatter: LineFormatter = self.toml_config.formatting()
        formatted_lines = line_formatter.format_lines(content)
        formatted_lines.insert(1, f"|{header}|")
        return formatted_lines

    @property
    def file_path(self) -> Path:
        _file: str = f"table_{self._table_index + 1}.md"
        return self._xml_document.table_dir.joinpath(_file)

    def write_to_file(self):
        if not self.file_path.exists():
            self.file_path.touch(exist_ok=True)

        with open(self.file_path, "w+", encoding="utf-8") as f:
            f.write(str(self))


class XmlTableRow(XmlFilePart):
    def __init__(self, xml_table: XmlTable, row_index: int):
        self._tag = "w:tr"
        super().__init__(self._tag, xml_table.content, row_index)
        self._index: int = row_index
        self._xml_table: XmlTable = xml_table
        self._content: EB = None

    @property
    def _cells(self) -> list[ElementBase]:
        return self.get_children_elements("w:tc")

    def __len__(self):
        return len(self._cells)

    def _cells_text(self) -> list[str]:
        final_text: list[str] = []

        for item in self._cells:
            p_text: list[str] = []

            p: ElementBase
            for p in item.iterdescendants(fqdn("w:p")):
                _text: list[str] = []
                t: ElementBase
                for t in p.iterdescendants(fqdn("w:t")):
                    _text.append(t.text)

                p_text.append("".join(_text))

            final_text.append("<br>".join(p_text))

        return final_text

    def cells_text(self) -> list[str]:
        return [get_all_text(item) for item in self._cells]


# class Formatting(StrEnum):
#     BOLD = "bold"
#     ITALIC = "italic"
#     SUPERSCRIPT = "superscript"
#     SUBSCRIPT = "subscript"
#     NONE = ""
#
#     def __repr__(self):
#         return f"{self.__class__.__name__}: {self._value_}"
#
#     __str__ = __repr__
#
#     @classmethod
#     def from_tag(cls, tag: str):
#         _conversion_dict: dict[str, str] = {
#             fqdn("w:b"): "bold",
#             fqdn("w:i"): "italic",
#             fqdn("superscript"): "superscript",
#             fqdn("subscript"): "subscript"
#         }
#         return cls(_conversion_dict.get(tag))


# def frame_line(line: str, formatting: Iterable[Formatting]):
#     formatting_dict: dict[Formatting, tuple[str, str]] = {
#         Formatting.BOLD: ("**", "**"),
#         Formatting.ITALIC: ("_", "_"),
#         Formatting.SUPERSCRIPT: ("<sup>", "</sup>"),
#         Formatting.SUBSCRIPT: ("<sub>", "</sub>"),
#         Formatting.NONE: ("", "")
#     }
#
#     for _format in formatting:
#         prefix, suffix = formatting_dict.get(_format)
#         line = f"{prefix}{line}{suffix}"
#     return line


# class XmlFormatter:
#     def __init__(self):
#         self._element: EB = None
#
#     def __bool__(self):
#         return self._element is not None
#
#     def __getitem__(self, item):
#         if isinstance(item, str):
#             return list(self._element.iterdescendants(fqdn(item)))
#         else:
#             raise KeyError
#
#     def find_children(self, element: EB = None, tags: Iterable[str] = None):
#         if tags is None:
#             return
#
#         if element is None:
#             element: ElementBase = self._element
#
#         child: ElementBase
#         _children_names: set[str] = set(child.tag for child in element.iterchildren())
#         return _children_names.intersection(tags)
#
#     def get_run_text(self, r: EB = None) -> dict[str, list[Formatting]]:
#         if r is None:
#             r = self._element
#
#         tags: tuple[str, ...] = (fqdn("w:b"), fqdn("w:i"), fqdn("w:vertAlign"))
#         r_pr: ElementBase = r.find(fqdn("w:rPr"), _ns)
#         t: ElementBase | None = r.find(fqdn("w:t"), _ns)
#
#         text: str = t.text if t is not None else ""
#
#         _formats: list[str] = [*self.find_children(r_pr, tags)]
#
#         if fqdn("w:vertAlign") in _formats:
#             vert_align: ElementBase = r_pr.find(fqdn("w:vertAlign"), _ns)
#             w_val: str = vert_align.get(fqdn("w:val"))
#             if w_val != "baseline":
#                 _formats.append(w_val)
#             _formats.remove(fqdn("w:vertAlign"))
#
#         if not _formats:
#             return {text: [Formatting.NONE]}
#         else:
#             return {text: [Formatting.from_tag(_format) for _format in _formats]}
#
#     def get_paragraph_text(self, p: EB = None):
#         if p is None:
#             p = self._element
#
#         r: ElementBase
#         lines: list[str] = [
#             frame_line(k, v)
#             for r in p.findall(fqdn("w:r"), _ns)
#             for k, v in self.get_run_text(r).items()]
#         return "".join(lines)
#
#     def get_all_text(self):
#         return "<br>".join(
#             self.get_paragraph_text(p)
#             for p in self._element.findall(fqdn("w:p"), _ns))
