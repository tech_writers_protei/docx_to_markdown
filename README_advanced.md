# docx_to_markdown User Guide Ru #

## Описание ##

Утилита извлекает и преобразует таблицы из docx-файла в формат Markdown. 
Все отформатированные таблицы сохраняются в отдельные md-файлы в директории согласно конфигурационному файлу ['config.toml'](docx_to_markdown/config/config.toml).

## Загрузка и запуск ##

Возможны варианты с использованием [git-проекта](#git-ru) целиком и исполняемым [bin-файлом](#bin-ru).

Далее в примерах предполагается, что в текущую директорию будет загружаться проект.

**Примечание.** Если указаны две команды, то первая --- для Windows-систем, вторая --- для \*nix-систем. 

### Загрузка и запуск <a name="git-ru">с помощью git</a> ###

1. Клонирование проекта.

        git clone --depth 1 https://gitlab.com/tech_writers_protei/docx_to_markdown.git

2. Запуск файла [/docx_to_markdown/main.py](./docx_to_markdown/main.py) из командной строки.

  * файл docx можно указать сразу, добавив абсолютный или относительный путь от текущей директории (`pwd`), в командной строке.

        python ./docx_to_markdown/docx_to_markdown/main.py <../path/to/file.docx>
        python3 ./docx_to_markdown/docx_to_markdown/main.py <../path/to/file.docx>

  * файл docx можно выбрать во всплывающем окне GUI после запуска.

        python ./docx_to_markdown/docx_to_markdown/main.py
        python3 ./docx_to_markdown/docx_to_markdown/main.py

### Загрузка и запуск <a name="bin-ru">с помощью исполняемого бинарного файла</a> ###

1. Переход в директорию для сохранения исполняемого файла.

```bash
cd <./path/to/directory>
```

2. Загрузка файла из директории [bin](./bin):

  * для Windows: [docx_to_markdown.exe](docx_to_markdown/bin/docx_to_markdown.exe);

  * для \*nix-систем: [docx_to_markdown](docx_to_markdown/bin/docx_to_markdown);

```bash
wget --output-document=docx_to_markdown.exe "https://gitlab.com/api/v4/projects/45566045/repository/files/bin%2Fdocx_to_markdown.exe/raw?ref=main"
wget --output-document=docx_to_markdown "https://gitlab.com/api/v4/projects/45566045/repository/files/bin%2Fdocx_to_markdown/raw?ref=main"
```

3. Запуск скрипта:

   * из Проводника/Терминала/... (Explorer/Finder/...);

   * из командной строки:
   
     * файл docx можно указать сразу, добавив абсолютный или относительный путь от текущей директории (`pwd`), в командной строке.

             ./docx_to_markdown.exe <../path/to/file.docx>
             sh ./docx_to_markdown <../path/to/file.docx>

     * файл docx можно выбрать во всплывающем окне GUI после запуска.
     
             ./docx_to_markdown.exe
             sh ./docx_to_markdown

## Создание исполняемого файла ##

Для создания наиболее актуального исполняемого файла возможно использовать следующий набор команд:

### Для Windows-систем ###

```bash
git clone --depth 1 https://gitlab.com/tech_writers_protei/docx_to_markdown.git
python -m pip install pyinstaller
cd ./docx_to_markdown/docx_to_markdown
pyinstaller --noconfirm --onefile --console --name "docx_to_markdown.exe" --distpath "../bin" \
--add-data "../config;config/" \
--add-data "../LICENSE;." \
--add-data "../MANIFEST.in;." \
--add-data "../pyproject.toml;." \
--add-data "../README.md;." \
--add-data "../README_techwriters.md;." \
--add-data "../requirements.txt;." \
--collect-binaries "." --paths "." --paths "../venv/Lib/site-packages" \
--hidden-import "lxml" --hidden-import "tomli" "./main.py"
```

Путь до файла `docx_to_markdown.exe` --- `<...>/docx_to_markdown/bin/docx_to_markdown.exe`.

### Для \*nix\-систем ###

```bash
git clone --depth 1 https://gitlab.com/tech_writers_protei/docx_to_markdown.git
python3 -m pip install pyinstaller
cd ./docx_to_markdown/docx_to_markdown
pyinstaller --noconfirm --onefile --console --name "docx_to_markdown" --distpath "../bin" \
--add-data "../config:config/" \
--add-data "../LICENSE:." \
--add-data "../MANIFEST.in:." \
--add-data "../pyproject.toml:." \
--add-data "../README.md:." \
--add-data "../README_techwriters.md:." \
--add-data "../requirements.txt:." \
--collect-binaries "." --paths "." --paths "../venv/lib/python3.11/site-packages" \
--hidden-import "lxml" --hidden-import "tomli" "./main.py"
```

Путь до файла `docx_to_markdown` --- `<...>/docx_to_markdown/bin/docx_to_markdown`.

**Примечание.** Для проверки, что текущая директория именно та, которая нужна после команды `cd`:

```bash
pwd
<...>/docx_to_markdown/docx_to_markdown
```

## Требования и сторонние зависимости ##

Необходимые условия и требования для работы:

* Python версии 3.8+;
* lxml версии 4.9.X;
* tomli версии 2.0.1 (если версия Python не старше 3.11);

## Файл конфигурации ##

Конфигурационный файл --- [config.toml](docx_to_markdown/config/config.toml).

### Параметры конфигурационного файла ###

* секция `[basic]` --- базовые параметры:
  * `folder_tables` --- директория для сохранения всех файлов Markdown. Путь может быть как абсолютным, так и относительным от docx-файла. По умолчанию --- `./tables`;
  * `folder_temp` --- временная директория для разархивирования docx-файла как ZIP-пакета. Путь может быть как абсолютным, так и относительным от docx-файла. По окончании работы директория удаляется. По умолчанию --- `./_temp`;
* секция `[formatting]` --- параметры форматирования:
  * `remove_multispaces` --- флаг замены множественных знаков табуляции на единичный. По умолчанию --- `true`;
  * `remove_spaces_before_dots` --- флаг удаления пробелов перед точками. По умолчанию --- `true`;
  * `escape_chars` --- флаг экранирования символов `*`, `_` и `-`. По умолчанию --- `true`;

# docx_to_markdown User Guide En #

## Description ##

The utility to transform tables from the docx file to the Markdown format. All Markdown-formatted tables are saved as separate md files to the folder specified in the config file ['config.toml'](docx_to_markdown/config/config.toml). 

## Get the repo and run ##

There are options through the [git project](#git-en) and the executable [bin file](#bin-en).

Further, the current directory is considered as a parent one for the downloaded project.

**Note.** If two commands are specified, the first command is implemented in the Windows system, and the second one is implemented in the \*nix systems. 

### <a name="git-en">Through the git</a> ###

1. Clone the project.

        git clone --depth 1 https://gitlab.com/tech_writers_protei/docx_to_markdown.git

2. Run the file [/docx_to_markdown/main.py](./docx_to_markdown/main.py) from the command line.

  * docx file may be specified at the moment if the absolute or relative path to the current directory (`pwd`) is added.

        python ./docx_to_markdown/docx_to_markdown/main.py <../path/to/file.docx>
        python3 ./docx_to_markdown/docx_to_markdown/main.py <../path/to/file.docx>

  * docx file may be specified in the popup GUI window after running.

        python ./docx_to_markdown/docx_to_markdown/main.py
        python3 ./docx_to_markdown/docx_to_markdown/main.py

### <a name="bin-en">Through the binary file</a> ###

1. Download file from the directory [bin](./bin):

  * Windows system: [docx_to_markdown.exe](docx_to_markdown/bin/docx_to_markdown.exe);

  * \*nix systems: [docx_to_markdown](docx_to_markdown/bin/docx_to_markdown);

```bash
wget --output-document=docx_to_markdown.exe "https://gitlab.com/api/v4/projects/45566045/repository/files/bin%2Fdocx_to_markdown.exe/raw?ref=main"
wget --output-document=docx_to_markdown "https://gitlab.com/api/v4/projects/45566045/repository/files/bin%2Fdocx_to_markdown/raw?ref=main"
```

2. Run the script:

   * from the Explorer/Finder/...;

   * from the command line:
   
     * docx file may be specified at the moment if the absolute or relative path to the current directory (`pwd`) is added.

             ./docx_to_markdown.exe <../path/to/file.docx>
             sh ./docx_to_markdown <../path/to/file.docx>

     * docx file may be specified in the popup GUI window after running.
     
             ./docx_to_markdown.exe
             sh ./docx_to_markdown

## Packaging ##

To package the most actual binary file, one may run the following commands:

### Windows system ###

```bash
git clone --depth 1 https://gitlab.com/tech_writers_protei/docx_to_markdown.git
python -m pip install pyinstaller
cd ./docx_to_markdown/docx_to_markdown
pyinstaller --noconfirm --onefile --console --name "docx_to_markdown.exe" --distpath "../bin" \
--add-data "../config;config/" \
--add-data "../LICENSE;." \
--add-data "../MANIFEST.in;." \
--add-data "../pyproject.toml;." \
--add-data "../README.md;." \
--add-data "../requirements.txt;." \
--collect-binaries "." --paths "." --paths "../venv/Lib/site-packages" \
--hidden-import "lxml" --hidden-import "tomli" "./main.py"
```

Path to the file `docx_to_markdown.exe` --- `<...>/docx_to_markdown/bin/docx_to_markdown.exe`.

### \*nix systems ###

```bash
git clone --depth 1 https://gitlab.com/tech_writers_protei/docx_to_markdown.git
python3 -m pip install pyinstaller
cd ./docx_to_markdown/docx_to_markdown
pyinstaller --noconfirm --onefile --console --name "docx_to_markdown" --distpath "../bin" \
--add-data "../config:config/" \
--add-data "../LICENSE:." \
--add-data "../MANIFEST.in:." \
--add-data "../pyproject.toml:." \
--add-data "../README.md:." \
--add-data "../requirements.txt:." \
--collect-binaries "." --paths "." --paths "../venv/Lib/site-packages" \
--hidden-import "lxml" --hidden-import "tomli" "./main.py"
```

Path to the file `docx_to_markdown` --- `<...>/docx_to_markdown/bin/docx_to_markdown`.

**Note.** To verify the current directory is the proper one after the `cd` command:

```bash
pwd
<...>/docx_to_markdown/docx_to_markdown
```

## Requirements and dependencies ##

Requirements and dependencies to run the utility:

* Python version 3.8+;
* lxml version 4.9.X;
* tomli version 2.0.1 (if the Python version is older than 3.11);

## Config file ##

Config file is [config.toml](docx_to_markdown/config/config.toml).

### Config file params ###

* section `[basic]` --- main parameters:
  * folder_tables --- the path to gather all final Markdown files. The path is either absolute or relative to the **docx** file. Default --- `./tables`;
  * folder_temp --- the path to extract the Word file as a ZIP archive. The path is either absolute or relative to the **docx** file. Finally, the folder is completely removed. Default --- `./_temp`;
* section `[formatting]` --- table content formatting parameters:
  * remove_multispaces --- flag to replace multiple spaces with the single one. Default --- `true`;
  * remove_spaces_before_dots --- flag to remove spaces before the dots. Default --- `true`;
  * escape_chars --- flag to add backslashes before the `*`, `_`, and `-` characters. Default --- `true`;
